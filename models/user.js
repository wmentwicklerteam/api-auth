'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    lastname: DataTypes.STRING(30),
    firstname: DataTypes.STRING(20),
    email: DataTypes.STRING(128),
    password: DataTypes.STRING(60),
    isAdmin: DataTypes.BOOLEAN
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};