const express = require('express');
const router = express.Router();
const users = require('../controllers/users');
router.get('/all', async (req, res) => {
    // for testing only ...
    let allUsers = [{
        id: 1,
        lastname: 'Kirrmann',
        firstname: 'Pius',
        email: 'pius.kirrmann@wolff-mueller.de',
        isAdmin: true
    }];
    res.send(allUsers);
})
module.exports = router;