const config = require('config');
const winston = require('winston');
const sqlite3 = require('sqlite3').verbose();
module.exports = function () {
    try {
        const dbsource = config.get('dbsource'); //TODO: check process.env.NODE_ENV
        const db = new sqlite3.Database(dbsource, sqlite3.OPEN_READONLY, err => {
            if (err) {
                throw new Error('Fatal Error: cannot open database');
            } else {
                db.close();
                winston.info('Checking database: SQLite database found ...');
            }
        });
    } catch (error) {
        throw new Error('Fatal Error: dbsource is not defined');
    }
}