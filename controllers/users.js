module.exports = {
    getAll: async () => {
        let users = [];
        try {
            users = await User.findAll();
        } catch (error) {
            throw ('Internal server error');
        }
        return users;
    }
}