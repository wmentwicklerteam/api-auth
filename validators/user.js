const Joi = require('@hapi/joi');
// https://github.com/hapijs/joi
function validateUser(user) {
    const schema = Joi.object().keys({
        lastname: Joi.string().min(2).max(30).required(),
        firstname: Joi.string().min(2).max(20).required(),
        email: Joi.string().email({
            minDomainSegments: 2
        }).required(),
        password: Joi.string().min(8).max(30).required()
    });
    return Joi.validate(user, schema);
};
module.exports = validateUser;